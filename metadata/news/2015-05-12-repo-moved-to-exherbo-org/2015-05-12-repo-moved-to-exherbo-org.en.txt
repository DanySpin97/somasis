Title: repository/somasis moved to git.exherbo.org
Author: Kylie McClain <somasis@exherbo.org>
Content-Type: text/plain
Posted: 2015-05-12
Revision: 1
News-Item-Format: 1.0

This repository is now located at git.exherbo.org, rather than GitHub, since I
have now been promoted to one of the core developers. The GitHub repository will
be not be updated anymore, and I'll probably be removing it within a few weeks.

To change the url, change the url in /etc/paludis/repositories/somasis.conf,

    sync = git+https://github.com/Somasis/exheres.git

should be changed to

    sync = git://git.exherbo.org/dev/somasis.git

